FROM python:3.8-slim

COPY . /root

WORKDIR /root

RUN pip install flask gunicorn numpy sklearn pandas python-dotenv natasha networkx matplotlib