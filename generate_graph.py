import os
import joblib
import numpy as np
import pandas as pd
import json
import re

# for plot
import base64
from io import BytesIO
import networkx as nx
import matplotlib.pyplot as plt

from flask import Flask, request, flash, jsonify, abort, redirect, url_for, render_template, send_file, after_this_request

from natasha import (
    Segmenter,
    MorphVocab,
    
    NewsEmbedding,
    NewsMorphTagger,
    NewsSyntaxParser,
    NewsNERTagger,
    
    PER,
    NamesExtractor,

    Doc
)

segmenter = Segmenter()
morph_vocab = MorphVocab()

emb = NewsEmbedding()
morph_tagger = NewsMorphTagger(emb)
syntax_parser = NewsSyntaxParser(emb)
ner_tagger = NewsNERTagger(emb)

names_extractor = NamesExtractor(morph_vocab)

app = Flask(__name__)

# выбор настроек среды(в данный момент почти пустой конфиг)
app.config.from_object('config.DevConfig')

# веб-интерфейс
@app.route('/')
def clear_format():
    return render_template('text_form.html')

# путь для запроса
@app.route('/get_text', methods = ['POST'])
def load_paint():
    """Return answer"""
    try:
        content = request.get_json()

        entities = []

        if len(content['input_text']) > 1:
            for text in content['input_text'].split('. '):
                if len(text.split(' ')) > 1:
                    result = get_entities_ru(text)
                    if result and result != None:
                        entities += result
        else:
            return jsonify("Error: empty enter string")

        str_content_entity = ""
        for entity in entities:
            str_content_entity += str(entity[0]) + " " + str(entity[2]) + " " + str(entity[1]) + "\n"

        result = {'str_content_entity':str_content_entity,'graph':plot_graph(entities)}

    except Exception as e:
        print('error')
        print(e)
        return redirect(url_for('bad_request'))

    return jsonify(result)

# страница ошибки
@app.route('/badrequest')
def bad_request():
    return abort(400)

# вспомогательные функции

# очистка текста
def text_clear(text):
    result = text.lower()
    result = re.sub(r'[!?]', '.', result)
    result = re.sub(r'[^а-яa-z\.\-\,]', ' ', result)
    result = re.sub(r"[\|/]", '', result) 
    result = re.sub(r"[\n]", ' ', result) 
    result = re.sub(r"[\r]", ' ', result) 
    result = re.sub(r"[nan]", ' ', result) 
    result = re.sub(r'\s+', ' ', result)
    result = re.sub(r' \.', '.', result)
    result = re.sub(r'\. \.', '.', result)
    result = re.sub(r'\.\.', '.', result)
    return result

def get_entities_ru(sent):
    doc = Doc(sent)
    doc.segment(segmenter)
    doc.parse_syntax(syntax_parser)
    doc.tag_ner(ner_tagger)
    doc.tag_morph(morph_tagger)

    root_id = False  

    for token in doc.tokens:
        try:
            token.lemmatize(morph_vocab)
        except:
            pass

    array_sent = {}

    for i in doc.tokens:
        if i.lemma != 'None':
            array_sent[i.id] = i.lemma
        else:
            array_sent[i.id] = i.text

    array_sent_head_id = {i.id: i.head_id for i in doc.tokens}

    # main action
    for i in doc.tokens:
      if i.rel == 'root':
        root_id = i.id
        break

    # cases
    array_result_new = []
    for i in doc.tokens:
      if i.rel == 'case' and i.head_id in array_sent_head_id and i.head_id in array_sent and array_sent_head_id[i.head_id] in array_sent:
        array_result_new.append([array_sent[array_sent_head_id[i.head_id]], array_sent[i.head_id], i.lemma])

    if not root_id and len(array_result_new) < 1:
        return False       

    array_result = [i.lemma for i in doc.tokens if i.head_id == root_id and i.rel != 'punct' and i.rel != 'case']

    # main
    if len(array_result) == 1:
        array_result_new.append([array_result[0], '', array_sent[root_id]])
    elif len(array_result) == 2:
        array_result_new.append([array_result[0], array_result[1], array_sent[root_id]])
    elif len(array_result) > 2:        
        for word_first_id in range(0,len(array_result)):
            for word_second_id in range(word_first_id + 1, len(array_result)):
                array_result_new.append([array_result[word_first_id], array_result[word_second_id], array_sent[root_id]])

    return array_result_new

def plot_graph(entity_pairs):
    # extract subject
    source = [i[0] for i in entity_pairs]

    # extract object
    target = [i[1] for i in entity_pairs]

    # relation
    relations = [i[2] for i in entity_pairs]

    kg_df = pd.DataFrame({'source':source, 'target':target, 'edge':relations})

    # create a directed-graph from a dataframe
    G=nx.from_pandas_edgelist(kg_df, "source", "target", create_using=nx.MultiDiGraph())

    fig = plt.figure(figsize=(12,12))

    pos = nx.spring_layout(G)
    nx.draw(G, with_labels=True, node_color='skyblue', edge_cmap=plt.cm.Blues, pos = pos)

    formatted_edge_labels = {(row['source'],row['target']):row['edge'] for index, row in kg_df.iterrows()} # use this to modify the tuple keyed dict if it has > 2 elements, else ignore
    nx.draw_networkx_edge_labels(G,pos,edge_labels=formatted_edge_labels,font_color='red')

    tmpfile = BytesIO()
    fig.savefig(tmpfile, format='png')
    encoded = base64.b64encode(tmpfile.getvalue()).decode('utf-8')

    html = '<img class="img-fluid" src=\'data:image/png;base64,{}\'>'.format(encoded)

    return html



    